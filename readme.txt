O presente projeto, desenvolvido por Adryan Alessandro Signor e Maria Eduarda Vanin Miotto, implementa um programa capaz de monitorar a execução de um outro programa.

Observações:
SO: linux;
Para compilação utilize o Makefile no diretório bin. Ex.: make install;
O arquivo de relatório do projeto encontra-se no diretório doc;
