#ifndef BENCHMARK_UTIL_H
#define BENCHMARK_UTIL_H

#include <stdio.h>
#include <unistd.h>

#define CAN_EXECUTE_PROGRAM 0
#define TRUE 1
#define FALSE 0

/**
 * Remove a element from array string
 * @param array
 * @param index
 * @param length
 */
void remove_element_array_string(char *array[], int index, int length);

/**
 * Validate program to be banchmarked
 * @param  argc
 * @param  argv
 * @return validation result
 */
int validate_input_args(int argc, char *argv[]);

#endif