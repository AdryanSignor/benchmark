#include "util.h"

void remove_element_array_string(char *array[], int index, int length) {
	for (int i = index; i < length - 1; i++) {
		array[i] = array[i + 1];
	}
}

int validate_input_args(int argc, char *argv[]) {
	int is_valid = FALSE;

	if (argc < 2) {
		printf("Usage: %s <program_path> [args]\n", argv[0]);
		fflush(stdout);
	} else if (access(argv[1], X_OK) == CAN_EXECUTE_PROGRAM) {
		is_valid = TRUE;
	} else {
		printf("Program \"%s\" not found or don't have permition to run\n", argv[1]);
		fflush(stdout);
	}

	return is_valid;
}