#include "system_info.h"

void show_system_cores() {
	printf("Physical cores: %d\n", get_nprocs());
	fflush(stdout);
}

void show_system_memory() {
	struct sysinfo info;
	sysinfo(&info);

	printf("Main memory size: %lu B\n", info.mem_unit * info.totalram);
	printf("Memory page size: %d B\n", getpagesize());
	fflush(stdout);
}

void show_system_info() {
	show_system_cores();
	show_system_memory();
}