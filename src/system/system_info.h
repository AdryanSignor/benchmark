#ifndef BENCHMARK_SYSTEM_INFO_H
#define BENCHMARK_SYSTEM_INFO_H

#include <stdio.h>
#include <sys/sysinfo.h>
#include <unistd.h>

/**
 * Print number of cores
 */
void show_system_cores();

/**
 * Print main memory size
 * Print memory page size
 */
void show_system_memory();

/**
 * Print number of cores
 * Print main memory size
 * Print memory page size
 */
void show_system_info();

#endif