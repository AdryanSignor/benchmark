#include "main.h"

void exec_benchmarking(char *path, char *argv[]) {
	pid_t pid_child = fork();

	if (pid_child == CHILD_PROCESS) {
		execv(path, argv);
	} else {
		int statusPtr;
		waitpid(pid_child, &statusPtr, WUNTRACED);

		show_system_info();
		show_child_process_info();
	}
}

int main(int argc, char *argv[]) {
	if (validate_input_args(argc, argv)) {
		remove_element_array_string(argv, 0, argc);
		argv[argc - 1] = NULL;
		exec_benchmarking(argv[0], argv);
		return EXIT_SUCCESS;
	} else {
		return EXIT_FAILURE;
	}
}