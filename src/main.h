#ifndef BENCHMARK_MAIN_H
#define BENCHMARK_MAIN_H

#include <sys/wait.h>
#include <stdlib.h>

#include "common/util.h"
#include "process/process_info.h"
#include "system/system_info.h"

#define CHILD_PROCESS 0

/**
 * Run program and print info
 * @param path
 * @param argv Arguments of the program to be executed
 */
void exec_benchmarking(char *path, char *argv[]);

#endif