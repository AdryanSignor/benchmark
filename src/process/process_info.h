#ifndef BENCHMARK_PROCESS_INFO_H
#define BENCHMARK_PROCESS_INFO_H

#include <stdio.h>
#include <sys/resource.h>

/**
 * Print the follow informations of first child
 * CPU time used in milliseconds
 * Context switch voluntarily
 * Context switch involuntarily
 * Page faults serviced that required I/O activity
 * Page faults serviced from the list of pages awaiting reallocation
 * Physical Memory used in bytes
 * Shared memory used in bytes
 * Unshared memory used for data in bytes
 * Unshared memory used for stack space in bytes
 */
void show_child_process_info();

#endif