#include "process_info.h"

void show_child_process_info() {
	struct rusage info;
	getrusage(RUSAGE_CHILDREN, &info);

	printf("CPU time used: %ld ms\n", (info.ru_utime.tv_sec + info.ru_stime.tv_sec) * 1000000 + info.ru_utime.tv_usec + info.ru_stime.tv_usec);
	printf("Context switch voluntarily: %ld\n", info.ru_nvcsw);
	printf("Context switch involuntarily: %ld\n", info.ru_nivcsw);
	printf("Page faults serviced that required I/O activity: %ld\n", info.ru_majflt);
	printf("Page faults serviced from the list of pages awaiting reallocation: %ld\n", info.ru_minflt);
	printf("Physical Memory used: %ld B\n", info.ru_maxrss * 1024);
	printf("Shared memory used: %ld B\n", info.ru_ixrss * 1024);
	printf("Unshared memory used for data: %ld B\n", info.ru_idrss * 1024);
	printf("Unshared memory used for stack space: %ld B\n", info.ru_isrss * 1024);
	fflush(stdout);
}